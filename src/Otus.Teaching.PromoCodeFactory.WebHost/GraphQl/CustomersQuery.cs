﻿using System.Linq;
using HotChocolate;
using HotChocolate.Data;
using HotChocolate.Types;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQl;

[ExtendObjectType(OperationTypeNames.Query)]
public class CustomersQuery
{
    [UseProjection]
    [UseFiltering]
    [UseSorting]
    public IQueryable<Customer> GetCustomers(
        [Service]  DataContext dbContext
    ) =>
        dbContext.Customers;
}